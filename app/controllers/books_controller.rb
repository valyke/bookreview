class BooksController < ApplicationController
	before_action :find_book, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user, only: [:new, :update, :edit]

	def index
		if params[:category].blank?
			@books = Book.all
			@categories = Category.all
		else
			@books = Book.where(category_id: params[:category])
			@book_category = Category.find(params[:category]).category
			@categories = Category.all
		end
	end

	def show
		@average = @book.review.average(:rating)
		@no_of_review = @book.review.count
	end

	def new
		@book = Book.new
	end

	def create
		@book = Book.new(book_params)
		@book.user_id = session[:account_id]
		if session[:account_id] == nil
			redirect_to sessions_new_path, notice: 'You must be logged in to Add A New Book!'
		else
			if @book.save
				redirect_to root_path, notice: 'Book Added!'
			else
				render 'new'
			end
		end
	end

	def edit
	end

	def update
		if @book.update(book_params)
			redirect_to book_path(@book)
		else
			render 'edit'
		end
	end

	def destroy
		@book.destroy
		redirect_to books_path
	end

	def add_review
		render 'form'
	end

	private
		def book_params
			params.require(:book).permit(
				:title, :description, :author, :category_id,
				:user_id, :book_img)
		end

		def find_book
			@book = Book.find(params[:id])
		end

		def authenticate_user
			if session[:account_id] == nil
				redirect_to sessions_new_path, notice: 'You must be logged in.'
			end
		end

end
