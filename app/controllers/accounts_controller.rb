class AccountsController < ApplicationController
	def index
		@accounts = Account.all
	end

	def new
		@account = Account.new
	end

	def create
		@account = Account.new(account_params)

		if @account.save
			redirect_to accounts_path, notice: 'Successful Sign Up!'
		else 
			render 'new'
		end
	end

	private 
		def account_params
			params.require(:account).permit(:name, :username, :password)
		end
end
