class ReviewsController < ApplicationController

	before_action :find_book
	before_action :find_review, only: [:edit, :update, :destroy]
	before_action :authenticate_user, only: [:new, :create]

	def index
		@book = @book_id
	end

	def new
		@review = Review.new
	end

	def create
		@review = Review.new(review_params)
		@review.user_id = session[:account_id]

		if @review.save
			redirect_to book_path(@book)
		else
			render 'new'
		end
		#@review.user_id = 
	end

	def edit
	end

	def update

		if @review.update(review_params)
			redirect_to book_path(@book)
		else
			render 'edit'
		end
	end

	def destroy
		@review.destroy
		redirect_to book_path(@book)
	end

	private
		def review_params
			params.require(:review).permit(:rating, :comment, :book_id)
		end

		def find_book
			@book = Book.find(params[:book_id])
		end

		def find_review
			@review = Review.find(params[:id])
		end

		def authenticate_user
			if session[:account_id] == nil
				redirect_to sessions_new_path, notice: 'You must be logged in to Post Review'
			end
		end


end
