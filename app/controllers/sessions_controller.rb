class SessionsController < ApplicationController
	def new
		@session = Account.new
	end

	def create
		#@session = Account.new(session_params)
		username = Account.where(username: params[:username]).first
		#if username && username.authenticate(params[:password])
		if username && username.authenticate(params[:password_digest])
			redirect_to books_path, notice: 'Logged IN'
			session[:isLogged] = 'logged'
			session[:account_id] = username.id
		else
			redirect_to sessions_new_path, notice: 'Invalid Username/Password'
		end


	end

	def destroy
		session[:isLogged] = nil
		session[:account_id] = nil
		redirect_to sessions_new_path, notice: 'Logged Out!'
	end

	# private
	# 	def session_params
	# 		params.require(:account).permit(:username, :password)
	# 	end
end
