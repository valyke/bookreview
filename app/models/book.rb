class Book < ActiveRecord::Base
	belongs_to :account
	belongs_to :category
	has_many :review

	validates_presence_of :title, :description, :author 

	mount_uploader :book_img, BookImgUploader	
	#validates_integrity_of :book_img


end
