class Account < ActiveRecord::Base
	validates_presence_of :name, :username, :password
	has_secure_password

	has_many :book
	has_many :review
end
