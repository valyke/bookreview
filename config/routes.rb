Rails.application.routes.draw do
	resources :books do
		resources :reviews
	end
	resources :accounts

	get 'sessions/index'
	get 'sessions/new'
	post 'sessions/create'
	get 'sessions/destroy'

	root 'books#index'
end
