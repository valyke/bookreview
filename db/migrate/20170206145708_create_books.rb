class CreateBooks < ActiveRecord::Migration
  def change
  	#drop_table :books
    create_table :books do |t|
      t.string :title
      t.text :description
      t.string :author

      t.timestamps
    end
  end
end
