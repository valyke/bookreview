class DropCategoriesTable < ActiveRecord::Migration
  def change
  	remove_column :books, :category_id
  	add_column :books, :category_id, :integer
  end
end
